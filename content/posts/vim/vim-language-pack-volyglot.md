+++
author = "Bala Wang"
title = "Vim syntax plugins - Volyglot (All in one solution)"
date = "2020-02-09T21:46:00+08:00"
description = "A solid language pack for Vim and perform good as plugins loaded on demand."
tags = [
  "Vim",
]
+++

![vim-polyglot](https://camo.githubusercontent.com/1f3898660ec02a98572ae4b7ea0e68b90f6a3f14/68747470733a2f2f692e696d6775722e636f6d2f395278514b366b2e706e67)

Here is the link: [https://github.com/sheerun/vim-polyglot](https://github.com/sheerun/vim-polyglot).

I was wondering a javascript plugin for my vim, this post [freshman.tech](https://freshman.tech/vim-javascript/) guides to Volyglot. Thanks for sharing!
