+++
author = "Bala Wang"
title = "Phpactor, a PHP completion, refactoring and introspection tool"
date = "2020-02-08T15:56:24+08:00"
description = "A quick guidance to setup phpactor vim plugin."
tags = [
  "Vim",
]
+++

If the PHP version is 7.1+ and you are a vimer (VIM 8 or NeoVIM), then, you should try [phpactor](https://github.com/phpactor/phpactor), a PHP completion, refactoring and introspection tool. 

## Recommended vimrc

* [amix/vimrc](https://github.com/amix/vimrc)

## Installation

The plugins listed here, it would better to go quickly over them to briefly understand how to use them and custom settings.

```

 git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
 sh ~/.vim_runtime/install_awesome_vimrc.sh
 
 # Plugins
 git clone https://github.com/ncm2/ncm2.git ~/.vim_runtime/my_plugins/ncm2
 git clone https://github.com/phpactor/ncm2-phpactor.git ~/.vim_runtime/my_plugins/ncm2-phpactor
 git clone https://github.com/phpactor/phpactor.git ~/.vim_runtime/my_plugins/phpactor
 git clone https://github.com/roxma/nvim-yarp.git ~/.vim_runtime/my_plugins/nvim-yarp
 git clone https://github.com/roxma/vim-hug-neovim-rpc.git ~/.vim_runtime/my_plugins/vim-hug-neovim-rpc
 
 cd ~/.vim_runtime/my_plugins/phpactor && composer install
 
 # Install neovim module, required for using ncm2 completion.
 python3 -m pip install neovim && python3 -m pip install pyvim
 
 # Install fzf (Fuzzy finder)
 git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
 ~/.fzf/install

```

## Couple of command functions help writing code

```
" Ensure the class name or code is under the cursor, then execute below commands

" Adds use statement of the selected class"
:call phpactor#UseAdd()

" Override a method from parent class
" Rename variable
" Rename class
" Rename class member
:call phpactor#ContextMenu()

" Impelements contracts/interface
" Adding missing assignment
" Complete contructor
" Fix namespace or classname
:call phpactor#Transform()

" Copy class
:call phpactor#CopyFile

" Class move
:call phpactor#MoveFile()

" Class new
:call phpactor#ClassNew()

```

More to find here: https://phpactor.github.io/phpactor/refactorings.html

## References

1. [Phpactor VIM plugin](https://phpactor.github.io/phpactor/vim-plugin.html)
2. [My brief example](https://github.com/BalaWxd/vimrc)
3. [Vim for PHP: The Complete Guide for a Powerful PHP IDE](https://thevaluable.dev/vim-php-ide/)
