+++
author = "Bala Wang"
title = "Update laradock workspace Ubuntu to 18.04 LTS"
date = "2020-02-12T23:44:00+08:00"
description = "How to upgrade laradock/workspace ubuntu version to 18.04 LTS."
tags = [
  "docker",
]
+++

Laradock workspace ([laradock/workspace](https://github.com/laradock/workspace)) is good for managing a PHP development environment using docker, especially using its full stack[^1]. 

However, I wanted to use Ubuntu 18.04, and its base image is using `phusion/baseimage:latest`, which is Ubuntu 16.04 LTS.

```
# Execute the command in workspace container
$ lsb_release -a

Distributor ID: Ubuntu
Description:    Ubuntu 16.04 LTS
...

```

And here is what phusion/baseimage says in its github repository README.md[^2] file.

```
# Use phusion/baseimage as base image. To make your builds reproducible, make
# sure you lock down to a specific version, not to `latest`!
# See https://github.com/phusion/baseimage-docker/blob/master/Changelog.md for
# a list of version numbers.

FROM phusion/baseimage:<VERSION>
```

## How to change laradock/workspace ubuntu to 18.04?

Update those Dockerfiles base image and change to something like this:

```
# I have forked the original source code, so reference to my repository
# file: https://github.com/BalaWxd/workspace-base/blob/2.6.1-ubuntu18.04/Dockerfile-74

# Change baseimage version to 0.11, refer to Ubuntu 18.04 LTS updates
FROM phusion/baseimage:0.11
```

Then, build this workspace image locally.

```
# I'm using PHP version 7.4, so the suffix shall be 7.4
$ docker build -t laradock/workspace:2.6.1-7.4 - < Dockerfile-74
```

Here is the tricky, laradock full stack's workspace is based on `laradock/workspace:2.6.1-${PHP_VERSION}`, say PHP_VERSION=7.4, as we have already built this image locally, it will load from local storage.

## Rename docker image tag name

I have changed `laradock/workspace:2.6.1` to `laradock/workspace-base`, to avoid name conflicts with laradock full stack's workspace..

```
$ docker tag laradock/workspace:2.6.1-7.4 laradock/workspace-base:2.6.1-7.4
```

I have change full stack's laradock workspace Dockerfile FROM statement as well. That's it.

```
# Build workspace
$ docker-compose -p myspace build workspace

# Start workspace container
$ docker-compose -p myspace up -d workspace

# Check workspace version
$ docker-compose -p myspace exec workspace lsb_release -a
```

## References

[^1]: [Laradock fullstack](https://github.com/laradock/laradock)
[^2]: [phusion/baseimage-docker](https://github.com/phusion/baseimage-docker#getting-started)
