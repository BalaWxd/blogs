+++
author = "Bala Wang"
title = "Docker delete unused images"
date = "2020-02-12T13:09:00+08:00"
description = "How to delete image has dependencies and unused images."
tags = [
  "docker",
]
+++

While the time goes, with creating docker containers, we probably created many images using disk spaces and we want to delete them.

## What is unused images

> "An unused image means that it has not been assigned or used in a container[^1]". 

## What is dangling image

> "A dangling image just means that you've created the new build of the image, but it wasn't given a new name[^1]". 

```
REPOSITORY  TAG     IMAGE ID      CREATED       SIZE
<none>      <none>  6618608d62e8  14 hours ago  1.73GB
```

## Comand to remove unused images[^2]

```
$ docker image prune --help
Usage:  docker image prune [OPTIONS]
Remove unused images
Options:
  -a, --all             Remove all unused images, not just dangling ones
      --filter filter   Provide filter values (e.g. 'until=<timestamp>')
  -f, --force           Do not prompt for confirmation
```

Examples:

```
$ docker image prune       # Remove just dangling ones
$ docker image prune --all # Remove all unused and dangling images
```

## If image to remove has dependent child image

> "conflict: unable to delete 69a57f6aca78 (cannot be forced) - image has dependent child image."

See discussion here: [Can't delete docker image with dependent child images](https://stackoverflow.com/questions/38118791/can-t-delete-docker-image-with-dependent-child-images).

## References

[^1]: [What is dangling image and what is an unused image](https://stackoverflow.com/questions/45142528/what-is-a-dangling-image-and-what-is-an-unused-image/45143234#45143234)
[^2]: [Docker image prune](https://docs.docker.com/engine/reference/commandline/image_prune/)
