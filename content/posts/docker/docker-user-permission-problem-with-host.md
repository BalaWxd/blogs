+++
author = "Bala Wang"
title = "Docker user permission problem with host"
date = "2020-02-07T20:42:24+08:00"
description = "How to fix docker container file permission problem not matching from host to docker container."
tags = [
  "docker",
]
+++

If you are using Linux and running a docker you could probably met my problem as well, when a file created in container stored in a mounted folder in host, you cannot edit the file from the host.

## How to resolve

Say, `ls -lah /path/to/file`, you will see the ownership of the file, like 500:500 on the host, ubuntu:ubuntu in the container. The cause is the host user's ID is different than the user in docker container. In my case, my host user is using `uid:500`, `gid:500`, but container is `uid:1000`, `gid:1000`.

Below commands can help changing the container's uid and gid.

```
# Switch to root
$ sudo su -

# Change uid and gid
$ usermod -u <host-uid> <username>
$ groupmod -g <host-gid> <groupname>

```

Or, if you can modify Dockerfile, you can either create the user or modify the user uses `adduser` or `usermod`, example:

```
$ adduser -D -G <somegroup> -u ${UID} <username>
$ usermod -o -u ${UID} -g <groupname> <username>
```

## References

[laradock](https://github.com/laradock/laradock/blob/master/workspace/Dockerfile)
