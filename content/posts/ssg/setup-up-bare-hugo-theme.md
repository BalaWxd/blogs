+++
author = "Bala Wang"
title = "Setup up bare hugo theme"
date = "2020-02-06T23:07:24+08:00"
description = "A brief specification for beginners to setup bare hugo theme"
tags = [
  "SSG",
]
+++

Besides from what's described in https://themes.gohugo.io/bare-hugo-theme/, there are 2 points I believe can be given some more specific for the new users uses [bare-hugo-theme](https://themes.gohugo.io/bare-hugo-theme/).

1. PostCss

> To enable this set params.postcss to true in your config.toml. Then, copy the package.json and the postcss.config.js into your repository and run npm install.

You will have to copy postcss.config.js and package.json to the root of the site, where config.toml exists. Run `npm install` will create `node_modules` directory. So, you got put this directory into `.gitignore` to avoid being revisioned. 

2. baseURL shall be set properly, otherwise stylesheets won't load properly.

For example, if running in developing `hugo server`, the baseURL defaults http://localhost:1313. You can use `hugo server --baseURL=http://dev.example.com --bind=0.0.0.0` to override the baseURL.

If you deploy to for example a host, run `hugo` the baseURL will be applied to the source. You can view source code in `public/index.html`, or check page source from browser, if you are using iPad and your site is exposed, you can try `https://validator.w3.org/check` to see the source of the page.

When using for example, gitlab pages, the baseURL is different while using custom domain. Gitlab page default domain is `https://\<user\>.gitlab.io/\<proj\>`.
