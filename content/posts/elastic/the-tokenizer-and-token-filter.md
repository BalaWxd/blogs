+++
author = "Bala Wang"
title = "Elasticsearch in finding similar electronic components by specificiations"
date = "2020-04-23T13:07:00+08:00"
description = "Using tokenizer and token filters to create a better search experience for electronic components specificiations."
tags = [
  "Elastic (ELK)",
]
+++

Given a text `MLCC,0805,22 uF,16V,±10%,C0G/NP0,X7R,Surface Mount,RoHS`, this is a sepcification description for a capacitor. Every word separated by the comma represents a property's value. 

* MLCC: Multiple layer ceramic capacitor.
* 0805: Case/package.
* 22uF or 22µF: Capacitance. The example is not very well written, since between 22 and uF has a whitespace.
* 16V: Voltage rating.
* Surface Mount: the mounting method.
* RoHS: Compliant.

This is one of the method we use at [NexPCB](https://www.nexpcb.com) to help identifying the component.

What if I want to find the similar capacitors? Like see if we can find a better alternatives. To break the specification text into multiple tokens (words) could be very helpful, like the properties listed above. 

Since the standard way elasticsearch does can break the text using non-alpha characters, but I want something more accurate and the similar components found shall have a 99% property in common.

## Custom tokenizer[^1] and token filters[^2]

1. Create a tokenizer that can break the text into tokens using comma (,), whitespace and back slash (/)
```
"tokenizer": {
  "spec_value_tokenizer": {
    "pattern": "(,| |/)",
    "type": "simple_pattern_split"
  }
}
```

*this still need to improve, since backslash can be part the measurement unit, like bps (b/s)*

2. Create a token filter using shingle to find "22 uF" and "22uF" using either of them (with and without whitespace)
```
"spec_value_shingle_filter": {
  "max_shingle_size": "2",
  "min_shingle_size": "2",
  "token_separator": " ",
  "type": "shingle"
}
```

*"22 uF", will get "22uF" and "22 uF", since 22 and uF and separate tokens, then shingle min and max size is 2*

3. Create another token filter using patter capture to extract the measurement unit and value
```
"spec_value_measurement": {
  "type": "pattern_capture",
  "preserve_original": "true",
  "patterns": [
    "^((?:\\+|-)?\\d*(?:\\.\\d+)?)\\s*((?:ppm/)?(?:°|deg|degree)?c?|(?:u|µ|p|n)?(?:f|a|v(?:/(?:k|μs))?)|m(?:b|b)?|b(?:ytes?)?|b(?:ps)?|db|cd|w|ohms?|Ω|(?:k?g)|(?:k|m)?hz?)$"
  ]
}
```

*Note: the regular expression is not covering every tech specification measurement for electronic components,*
*if you have better or more complete solution, please send me an email at xiaodan.w@outlook.com, thanks!*

## Use minimum_should_match parameter to improve match result[^3]

```
POST /components/_search
{
  "query": {
    "query_string": {
      "query": "MLCC,0805,22 uF,16V,±10%,C0G/NP0,X7R,Surface Mount,RoHS",
      "default_field": "specification",
      "minimum_should_match": "99%"
    }
  }
}
```

https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-minimum-should-match.html

## Validate the implementation

```
POST /components/_analyze
{
  "analyzer": "part_specification_string",
  "text": "MLCC,0805,22 uF,16V,±10%,C0G/NP0,X7R,Surface Mount,RoHS"
}
```

```
# Output

{
  "tokens" : [
    {
      "token" : "mlcc",
      "start_offset" : 0,
      "end_offset" : 4,
      "type" : "word",
      "position" : 0
    },
    {
      "token" : "mlcc 0805",
      "start_offset" : 0,
      "end_offset" : 9,
      "type" : "shingle",
      "position" : 0,
      "positionLength" : 2
    },
    {
      "token" : "0805",
      "start_offset" : 5,
      "end_offset" : 9,
      "type" : "word",
      "position" : 1
    },
    {
      "token" : "0805 22",
      "start_offset" : 5,
      "end_offset" : 12,
      "type" : "shingle",
      "position" : 1,
      "positionLength" : 2
    },
    {
      "token" : "22",
      "start_offset" : 10,
      "end_offset" : 12,
      "type" : "word",
      "position" : 2
    },
    {
      "token" : "22 uf",
      "start_offset" : 10,
      "end_offset" : 15,
      "type" : "shingle",
      "position" : 2,
      "positionLength" : 2
    },
    {
      "token" : "uf",
      "start_offset" : 13,
      "end_offset" : 15,
      "type" : "word",
      "position" : 3
    },
    {
      "token" : "uf 16v",
      "start_offset" : 13,
      "end_offset" : 19,
      "type" : "shingle",
      "position" : 3,
      "positionLength" : 2
    },
    {
      "token" : "16v",
      "start_offset" : 16,
      "end_offset" : 19,
      "type" : "word",
      "position" : 4
    },
    {
      "token" : "16v 16",
      "start_offset" : 16,
      "end_offset" : 19,
      "type" : "shingle",
      "position" : 4,
      "positionLength" : 2
    },
    {
      "token" : "16",
      "start_offset" : 16,
      "end_offset" : 19,
      "type" : "word",
      "position" : 5
    },
    {
      "token" : "16 v",
      "start_offset" : 16,
      "end_offset" : 19,
      "type" : "shingle",
      "position" : 5,
      "positionLength" : 2
    },
    {
      "token" : "v",
      "start_offset" : 16,
      "end_offset" : 19,
      "type" : "word",
      "position" : 6
    },
    {
      "token" : "v ±10%",
      "start_offset" : 16,
      "end_offset" : 24,
      "type" : "shingle",
      "position" : 6,
      "positionLength" : 2
    },
    {
      "token" : "±10%",
      "start_offset" : 20,
      "end_offset" : 24,
      "type" : "word",
      "position" : 7
    },
    {
      "token" : "±10% c0g",
      "start_offset" : 20,
      "end_offset" : 28,
      "type" : "shingle",
      "position" : 7,
      "positionLength" : 2
    },
    {
      "token" : "c0g",
      "start_offset" : 25,
      "end_offset" : 28,
      "type" : "word",
      "position" : 8
    },
    {
      "token" : "c0g np0",
      "start_offset" : 25,
      "end_offset" : 32,
      "type" : "shingle",
      "position" : 8,
      "positionLength" : 2
    },
    {
      "token" : "np0",
      "start_offset" : 29,
      "end_offset" : 32,
      "type" : "word",
      "position" : 9
    },
    {
      "token" : "np0 x7r",
      "start_offset" : 29,
      "end_offset" : 36,
      "type" : "shingle",
      "position" : 9,
      "positionLength" : 2
    },
    {
      "token" : "x7r",
      "start_offset" : 33,
      "end_offset" : 36,
      "type" : "word",
      "position" : 10
    },
    {
      "token" : "x7r surface",
      "start_offset" : 33,
      "end_offset" : 44,
      "type" : "shingle",
      "position" : 10,
      "positionLength" : 2
    },
    {
      "token" : "surface",
      "start_offset" : 37,
      "end_offset" : 44,
      "type" : "word",
      "position" : 11
    },
    {
      "token" : "surface mount",
      "start_offset" : 37,
      "end_offset" : 50,
      "type" : "shingle",
      "position" : 11,
      "positionLength" : 2
    },
    {
      "token" : "mount",
      "start_offset" : 45,
      "end_offset" : 50,
      "type" : "word",
      "position" : 12
    },
    {
      "token" : "mount rohs",
      "start_offset" : 45,
      "end_offset" : 55,
      "type" : "shingle",
      "position" : 12,
      "positionLength" : 2
    },
    {
      "token" : "rohs",
      "start_offset" : 51,
      "end_offset" : 55,
      "type" : "word",
      "position" : 13
    }
  ]
}

```

## References

[^1]: [Tokenizers](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-tokenizers.html)
[^2]: [Token filter](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-tokenfilters.html)
[^3]: [minimum_should_match](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-minimum-should-match.html)
[^4]: [Tokenizer vs token filters](https://stackoverflow.com/questions/37168764/tokenizer-vs-token-filters)
