+++
author = "Bala Wang"
title = "Quick guide to composer install drupal"
date = "2020-02-15T10:15:00+08:00"
description = "A brief specification for beginners to setup bare hugo theme"
draft = true
tags = [
  "Drupal",
]
+++

Skip the long documentation and assuming already knowing composer and all prerequisites prepared.

```
# Replace my_site_name with what ever you want!
export DRUPAL_SITE_NAME=<my_site_name>
composer create-project drupal-composer/drupal-project:8.x-dev --stability dev --no-interaction $DRUPAL_SITE_NAME -vvv
cd $DRUPAL_SITE_NAME
```

