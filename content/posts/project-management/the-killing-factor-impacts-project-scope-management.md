---
author: "Bala Wang"
title: "Manage project scope properly can help project delivery"
date: 2020-02-18T22:05:00+08:00
description: "Poor project management is a factor causing project failing. Learn to incubating ideas and do code refactoring in daily bases is helpful."
tags: [ "project management" ]
---

Which of the following you think is the top one risk can fail a project?

1. Number of developers (different maturity levels) in the project?
2. Complexity of integrations in the application?
3. No clear picture of the final application look & feel?
4. Unmanageable backlog and scope?

If having a really small development team, say maybe just 2 or 3. The focus on making something useful is really more important than making a less perfect one. A word saying, think big, start small, move fast!

In a Agile software development management, we are welcome "changes", but what can be the major reason for failing a project, if not managing the changes properly!

So, I would put **Unmanageable backlog and scope** on the top one risk that causes a project failure. In how many times, we keep adding features and code improvements tasks to the sprint those change the goal and milestone plans?

* Estimate a feature too rough, lately, developer starts adding his own ideas to it, instead of re-estimate or try to separate into smaller ones
* New idea which looks good and started a task for it, instead of incubating it in another list
* Think too much on UX by adding functions try to complete more, instead of thinking how to minimize the complexity of interactions
* Keep optimizing code and rewrite without a clear architecture or implements a architecture design partially

All of that reflects a poor project management.

We need a incubator for those not planed features, we don't add them into the release sprint until we plan them.
For code improvements, instead of puting in incubator list, it is better to do code refactoring in a daily bases, to improve the code quality instead of waiting to optimize, since the more we create the harder to improve. Then, we need to build a stronger team, with highly autonomy and try test driven development and estiblish DevOps. This is also helpful to grow the skills of the team.

Don't focus on the feature, focus on the values and eliminate wastes, is just like do less is more.
