---
title: "7 Tips Mitigating Risks Over Gaps Between Contractor"
date: 2020-10-24T21:50:47+08:00
year: 2020
tags: [ "project management" ]
---

The gaps between product idea and reality is common. It will never get so real before we actual saw and used it. Here I come up with some tips, which might be useful to think ahead or help mitigating significant risks.

1. Knowledge map. Gather the knowledges needed for the project and map to your resources, see if there is gap and you can fill it up someday.
2. Clear timeline. Clear timeline, helps team create critical milestones, which pushes to the target date; or, we can also assess if the standards are sufficient for this timeline (not too hard)
3. Ready to contribute. If you are the owner of the product, make sure you will help the team any time.
4. Have a “iteratively improving” mindset. Try to avoid those not critical features, test only what really matters first (MVP), then improve based on that and feedbacks. Most of the time, we can over-design from many perspectives.
5. Commit to deadlines. If we keep missing the deadline of the milestones, can be messed up with our other plans, once it happened, we need to seriously find the root causes and improve that.
6. Open and professional to problems. Anger is a signal to serious problems, but open to discuss it helps team grow.
7. Confirm assumptions and talk daily if needed.

*Originally posted on [linkedin](https://www.linkedin.com/posts/balawxd_the-gaps-between-product-idea-and-reality-activity-6725743225287663617-aunv).*
