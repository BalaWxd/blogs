---
author: "Bala Wang"
title: "Setup laravel, socket.io and vuejs"
date: "2020-03-06T18:31:00+08:00"
description: "A quick guide that uses laravel-echo-server and socket.io-client example for vuejs and laravel project."
tags: ["Dev"]
year: 2020
---

## References

1. [https://github.com/MetinSeylan/Vue-Socket.io](https://github.com/MetinSeylan/Vue-Socket.io)
2. [https://github.com/tlaverdure/laravel-echo-server](https://github.com/tlaverdure/laravel-echo-server)
3. [https://www.freecodecamp.org/news/how-to-use-laravel-with-socket-io-e7c7565cc19d/](https://www.freecodecamp.org/news/how-to-use-laravel-with-socket-io-e7c7565cc19d/)
