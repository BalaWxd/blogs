+++
author = "Bala Wang"
title = "Install lib from a git repository of a certain branch"
date = "2020-05-28T17:16:00+08:00"
description = "npm install from a git repository of a specific branch."
tags = [
  "Dev",
]
+++

Assuming the registerred version has a bug fixed in its development branch, where we want to try that with npm install. Here is an example:

```
# Install acme-dns-01-godaddy plugin for greenlock express challenge plugin from "fix" branch.
npm i git+https://git.rootprojects.org/root/acme-dns-01-godaddy.js.git#fix
```

You have to ensure the address of the repository has been prefixed with `git+` and suffixed with `#<branch>`. 
