+++
author = "Bala Wang"
title = "Grafana docker environment using docker-compose"
date = "2020-02-15T10:15:00+08:00"
description = "A brief instruction for beginners to install grafana using docker"
tags = [
  "visualization",
  "docker"
]
+++

The blog is a documentation of simply installing grafana dashboard tool for using docker-compose and managing the environment viriables in a .env file.

Here is my example code: https://github.com/BalaWxd/grafana-docker.

Default login credentials: 

**Username**: `admin` \
**Password**: `admin`

# References

1. [Run Grafana Docker image](https://grafana.com/docs/grafana/latest/installation/docker/) 
2. [Grafana Github Dockerfiles](https://github.com/grafana/grafana/tree/master/packaging/docker/custom)
